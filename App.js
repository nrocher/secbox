import React from 'react';
import * as Font from 'expo-font';
import { DeviceEventEmitter, Text, View } from 'react-native';
import RNSecureStorage from 'rn-secure-storage';
import firebase from 'react-native-firebase';

import { Actions, Router, Scene, Stack } from 'react-native-router-flux';
import LogIn from './APP/Auth/LogIn';
import SignUp from './APP/Auth/SignUp';
import LostPassword from './APP/Auth/LostPassword';

import Home from './APP/Messenger/Home';

import ValidNewAvatar from './APP/User/ValidNewAvatar';

import Settings from './APP/User/Settings';
import ChangeAvatar from './APP/User/ChangeAvatar';
import ChangeGeneralInformation from './APP/User/ChangeGeneralInformation';
import ChangePassword from './APP/User/ChangePassword';

import AddFriend from './APP/Friend/AddFriend';
import Friend from './APP/Friend/Friend';


import Caller from './APP/Caller/Caller';

import USER_API from './UTILS/User';

export default class App extends React.Component {
    componentDidMount() {
        this._loadAssets();






        RNSecureStorage.get('token')
            .then(async token => {

                const enabled = await firebase.messaging().hasPermission();
                if (!enabled) {
                    try {
                        await firebase.messaging().requestPermission();
                    } catch(err){
                        console.error("CANNOT GET MESSAGING")
                    }
                }

                const fcmToken = await firebase.messaging().getToken();
                if (fcmToken) {

                    USER_API.updatePhoneId(fcmToken).then(_ => {
                        //console.log("TOKEN mis a jour")
                    }).catch(err => {
                        console.error("Token non mis a jour", err)
                    })

                } else {
                    console.error("PAS DE TOKEN FCM")
                }

                Actions.reset('messenger');
            })
            .catch(err => {
                Actions.reset('auth');
            });
    }

    _loadAssets = async _ => {
        try {
            await Font.loadAsync({
                'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
                'open-sans-regular': require('./assets/fonts/OpenSans-Regular.ttf'),
                'open-sans-semibold': require('./assets/fonts/OpenSans-SemiBold.ttf'),
            });
        } catch (e) {
            console.error(e);
        } finally {
        }
    };

    render() {
        return (
            <Router>
                <Stack key='root' hideNavBar>
                    <Scene key='loading' component={Loading} title='Loading' initial />

                    <Stack key='auth' hideNavBar>
                        <Scene key='login' component={LogIn} title='Login' initial />
                        <Scene key='signup' component={SignUp} title='SignUp' />
                        <Scene key='lostpassword' component={LostPassword} title='LostPassword' />
                    </Stack>

                    <Stack key='messenger' hideNavBar>
                        <Scene key='home' component={Home} title='Home' initial />

                        <Scene key='settings' component={Settings} title='Settings' />
                        <Scene key='changeGeneralInformation' component={ChangeGeneralInformation} title='Change General Information' />
                        <Scene key='changePassword' component={ChangePassword} title='Change Password' />
                        <Scene key='changeAvatar' component={ChangeAvatar} title='Change Avatar' />

                        <Scene key='validNewAvatar' component={ValidNewAvatar} title='validNewAvatar' />
                        <Scene key='addFriend' component={AddFriend} title='AddFriend' />
                        <Scene key='friend' component={Friend} title='Friend' />


                        <Scene key='caller' component={Caller} title='Caller'  />
                    </Stack>
                </Stack>
            </Router>
        );
    }
}

class Loading extends React.PureComponent {
    render() {
        return (
            <View style={{ backgroundColor: 'white', flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontFamily: 'open-sans-bold', fontSize: 13, color: '#000' }}>Chargement</Text>
            </View>
        );
    }
}
