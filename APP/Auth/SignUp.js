import React from 'react';
import { Alert, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { MaterialIcons } from '@expo/vector-icons';
import API_AUTH from '../../UTILS/Auth';

import styles from './Style';
import Button from '../Basic/Button';
import Input from '../Basic/Input';
import Link from '../Basic/Link';
export default class SignUp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            buttonClicked: false,
        };
    }

    _navigateToLogIn = _ => Actions.pop();

    _signup = _ => {
        this.setState({ buttonClicked: true });
        if (this.email.isValid() && this.motdepasse.isValid() && this.pseudo.isValid() && this.nom.isValid() && this.prenom.isValid()) {
            API_AUTH.signup(this.email.value(), this.pseudo.value(), this.motdepasse.value(), this.nom.value(), this.prenom.value())
                .then(_ => {
                    Alert.alert('Inscription', 'Vous êtes maintenant inscrit !', [
                        {
                            text: 'Connexion',
                            onPress: this._navigateToLogIn,
                        },
                    ]);
                })
                .catch(err => {
                    this.setState({ buttonClicked: false });
                    Alert.alert('Inscription', err);
                });
        } else {
            Alert.alert('Inscription', 'Veuillez remplir tous les champs correctement.');
            this.setState({ buttonClicked: false });
        }
    };

    render() {
        return (
            <KeyboardAwareScrollView>
                <View style={{ ...styles.container, paddingVertical: 25 }}>
                    <MaterialIcons name='local-phone' size={75} color='#2487E9' />
                    <Text style={styles.Logo}>DuoCall</Text>

                    <Input
                        name='Prénom'
                        placeholder='Votre prénom'
                        autoCapitalize={'words'}
                        ref={i => (this.prenom = i)}
                        onSubmitEditing={_ => this.nom.focus()}
                        returnKeyType={'next'}
                    />

                    <Input
                        name='Nom'
                        placeholder='Votre nom'
                        autoCapitalize={'words'}
                        ref={i => (this.nom = i)}
                        onSubmitEditing={_ => this.email.focus()}
                        returnKeyType={'next'}
                    />

                    <Input
                        name='Email'
                        placeholder='Votre email'
                        ref={i => (this.email = i)}
                        keyboardType={'email-address'}
                        returnKeyType={'next'}
                        onSubmitEditing={_ => this.pseudo.focus()}
                    />

                    <Input
                        name='Pseudo'
                        placeholder='Votre pseudo'
                        ref={i => (this.pseudo = i)}
                        returnKeyType={'next'}
                        onSubmitEditing={_ => this.motdepasse.focus()}
                    />

                    <Input name='Mot de passe' placeholder='************' ref={i => (this.motdepasse = i)} secureTextEntry={true} />

                    <Button
                        color='blue'
                        onPress={this._signup}
                        pressed={this.state.buttonClicked}
                        pressedText='Veuillez patienter'
                        text='Inscription'
                    />

                    <Link text='Vous avez un compte ?' onPress={this._navigateToLogIn} />
                </View>
            </KeyboardAwareScrollView>
        );
    }
}
