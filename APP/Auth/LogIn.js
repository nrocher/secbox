import React from 'react';
import { Alert, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import RNSecureStorage from 'rn-secure-storage';

import { MaterialIcons } from '@expo/vector-icons';
import API_AUTH from '../../UTILS/Auth';

import styles from './Style';
import Button from '../Basic/Button';
import Input from '../Basic/Input';
import Link from '../Basic/Link';

export default class LogIn extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            buttonClicked: false,
        };
    }

    _navigateToSignUp = _ => Actions.push('signup');

    _navigateToLostPassword = _ => Actions.push('lostpassword');

    _connexion = _ => {
        this.setState({ buttonClicked: true });
        if (this.email.isValid() && this.motdepasse.isValid()) {
            API_AUTH.login(this.email.value(), this.motdepasse.value())
                .then(token => {
                    RNSecureStorage.set('token', token, {}).then(
                        res => {
                            Actions.reset('messenger');
                        },
                        err => {
                            console.error(err);
                            this.setState({ buttonClicked: false });
                            Alert.alert('Connexion', 'Erreur système.');
                        },
                    );
                })
                .catch(err => {
                    this.setState({ buttonClicked: false });
                    Alert.alert('Connexion', err);
                });
        } else {
            Alert.alert('Connexion', 'Veuillez remplir tous les champs correctement.');
            this.setState({ buttonClicked: false });
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <MaterialIcons name='local-phone' size={75} color='#2487E9' />
                <Text style={styles.Logo}>DuoCall</Text>

                <Input
                    name='Email'
                    placeholder='Votre email'
                    ref={input => (this.email = input)}
                    keyboardType={'email-address'}
                    returnKeyType={'next'}
                    onSubmitEditing={_ => this.motdepasse.focus()}
                />

                <Input
                    name='Mot de passe'
                    placeholder='************'
                    ref={input => (this.motdepasse = input)}
                    onSubmitEditing={this._connexion}
                    secureTextEntry={true}
                />

                <Link inputWidth={true} text='Mot de passe oublié ?' onPress={this._navigateToLostPassword} />

                <Button color='blue' onPress={this._connexion} pressed={this.state.buttonClicked} pressedText='Veuillez patienter' text='Connexion' />

                <Link text="S'inscrire" onPress={this._navigateToSignUp} />
            </View>
        );
    }
}
