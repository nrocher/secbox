import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    subText: {
        width: '80%',
        marginBottom: 15,
        lineHeight: 20,
    },
    Logo: {
        fontFamily: 'open-sans-bold',
        fontSize: 30,
        color: '#2487E9',
        marginBottom: 25,
    }
});
