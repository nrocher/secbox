import React from 'react';
import { Alert, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { MaterialIcons } from '@expo/vector-icons';

import styles from './Style';
import Button from '../Basic/Button';
import Input from '../Basic/Input';
import Link from '../Basic/Link';

export default class LostPassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            buttonClicked: false,
        };
    }

    _navigateToLogIn = _ => Actions.pop();

    _lostPassword = _ => {
        this.setState({ buttonClicked: true });
        if (this.email.isValid()) {
            setTimeout(_ => {
                Alert.alert('Mot de passe oublié', "Non implementé eheh !\nFallait pas l'oublier.");
                this.setState({ buttonClicked: false });
            }, 500);
        } else {
            Alert.alert('Mot de passe oublié', 'Veuillez remplir tous les champs correctement.');
            this.setState({ buttonClicked: false });
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <MaterialIcons name='local-phone' size={75} color='#2487E9' />
                <Text style={styles.Logo}>DuoCall</Text>

                <Input name='Email' placeholder='Votre email' ref={i => (this.email = i)} keyboardType={'email-address'} />

                <Text style={styles.subText}>Un email vous sera envoyé avec un nouveau mot de passe si votre compte existe.</Text>

                <Button
                    color='blue'
                    onPress={this._lostPassword}
                    pressed={this.state.buttonClicked}
                    pressedText='Veuillez patienter'
                    text='Envoyer'
                />

                <Link text='Connexion' onPress={this._navigateToLogIn} />
            </View>
        );
    }
}
