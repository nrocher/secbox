import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        width: '100%',
        minHeight: '100%',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    ContainerHeader: {
        width: '100%',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    Header: {
        flexDirection: 'row',
        width: '100%',
        padding: 15,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    HeaderTitle: {
        fontFamily: 'open-sans-bold',
        fontSize: 18,
        color: '#2487E9',
    },
    HeaderIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 35,
        height: 35,
    },
    ListHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        paddingHorizontal: 15,
        marginTop: 50,
        marginBottom: 15,
    },
    ListHeaderText: {
        fontFamily: 'open-sans-semibold',
        fontSize: 18,
        color: '#000',
    },
    HeaderIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 40,
        height: 40,
    },
    UserInfoavatar: {
        width: 200,
        height: 200,
        textAlign: 'center',
        lineHeight: 200,
        borderRadius: 500,
        backgroundColor: '#E0E0E0',
    },
    UserInfo: {
        alignItems: 'center',
        flexDirection: 'column',
    },
    UserInfoTitle: {
        fontFamily: 'open-sans-bold',
        fontSize: 20,
        color: '#000',
        marginTop: 15,
    },
    UserInfoPseudo: {
        fontFamily: 'open-sans-regular',
        fontSize: 15,
        color: '#000',
        marginTop: 5,
    },





    UserBar: {
        flexDirection: 'row',
    },
    UserBarName: {
        width: '100%',
        maxWidth: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    UserBarBloc: {
        flex:1,
        flexDirection: 'row',
        alignItems: 'center',
        overflow: "hidden"
    },
    UserBarTitle: {
        flex:1,
        fontFamily: 'open-sans-bold',
        fontSize: 18,
        color: '#000',
        marginLeft: 15,
        paddingRight: 15,
    },
    UserBaravatar: {
        borderRadius: 100,
        backgroundColor: '#E0E0E0',
        textAlign: 'center',
        height: 50,
        lineHeight: 50,
        width: 50,
    },
    voirButton: {
        elevation: 0,
        borderColor: '#2487E9',
        borderWidth: 2,
        paddingVertical:5,
        paddingHorizontal:10
    }
});
