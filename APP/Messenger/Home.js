import React from 'react';
import { DeviceEventEmitter, Alert, Text, FlatList, TouchableNativeFeedback, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

import firebase from 'react-native-firebase';

import { MaterialIcons } from '@expo/vector-icons';

import Avatar from '../Basic/Avatar';
import Button from '../Basic/Button';

import styles from './Style';

import USER_API from '../../UTILS/User';

export default class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            nom: '',
            prenom: '',
            avatar: {},
            id: '',
            pseudo: '',
            bio: '',
            friends: [],
            loading: true,
        };
    }

    componentDidMount() {




        DeviceEventEmitter.addListener('WakeUpApp', (data) => {

            console.warn("WakeUpApp")
            console.warn("WakeUpApp")
            console.warn("WakeUpApp")
            console.warn("WakeUpApp")
            console.warn(data)

            Actions.reset(data.action, data.props)

        });

        DeviceEventEmitter.addListener('wakeUpApp', (data) => {

            console.warn("wakeUpApp")
            console.warn("wakeUpApp")
            console.warn("wakeUpApp")
            console.warn("wakeUpApp")
            console.warn(data)

            Actions.reset(data.action, data.props)

        });












        this.loadUserData();
        this.loadFriends();

        firebase.messaging().onMessage(message => {
            if(message._data.messageType == "call") {
                if(message._data.asking) {
                    USER_API.getUserInformation(message._data.asking)
                    .then(user => {
                        Actions.push('caller', { RTCdescription: message._data, ...user, type: 'receiving' });
                    })
                    .catch(err => {
                        Alert.alert('SecBox', err);
                    });
                }
            }
        });
    }

    loadFriends = _ => {
        this.setState({ loading: true });
        USER_API.getFriends()
            .then(friends => {
                this.setState({ loading: false, friends: friends.filter(fr => fr.statut == 1 || (fr.statut == 0 && fr.asked == false)) });
            })
            .catch(err => {
                Alert.alert('SecBox', err);
            });
    };

    loadUserData = _ => {
        USER_API.getUserInformation()
            .then(user => {
                this.setState(user);
            })
            .catch(err => {
                Alert.alert('SecBox', err);
            });
    };

    // TODO : Remove this
    freshFriends = _ => {
        this.loadFriends();
    };

    // TODO : Remove this
    freshAvatar = _ => {
        this.loadUserData();
    };

    onMorePress = _ => Actions.push('settings', { userInfo: this.state });

    addFriend = _ => Actions.push('addFriend', { pseudo: this.state.pseudo });

    render() {
        return (
            <FlatList
                contentContainerStyle={styles.container}
                ListHeaderComponentStyle={styles.ContainerHeader}
                data={this.state.friends}
                ListHeaderComponent={_ => (
                    <>
                        <View style={styles.Header}>
                            <Text style={styles.HeaderTitle}>SecBox</Text>
                            <TouchableNativeFeedback onPress={this.onMorePress} background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                                <View style={styles.HeaderIcon}>
                                    <MaterialIcons name='more-vert' size={25} />
                                </View>
                            </TouchableNativeFeedback>
                        </View>

                        <View style={styles.UserInfo}>
                            <Avatar style={styles.UserInfoavatar} fast iconSize={75} img={this.state.avatar['300']} />
                            <Text style={styles.UserInfoTitle}>
                                {this.state.prenom} {this.state.nom}
                            </Text>
                            <Text style={styles.UserInfoPseudo}>{this.state.pseudo}</Text>
                            {this.state.bio != '' ? <Text style={styles.UserInfoPseudo}>{this.state.bio}</Text> : null}
                        </View>

                        <View style={styles.ListHeader}>
                            <Text style={styles.ListHeaderText}>Mes contacts</Text>
                            <TouchableNativeFeedback onPress={this.addFriend} background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                                <View style={styles.HeaderIcon}>
                                    <MaterialIcons name='person-add' size={25} color='#424242' />
                                </View>
                            </TouchableNativeFeedback>
                        </View>
                    </> /* */
                )}
                ListEmptyComponent={<Text>{this.state.loading ? 'Chargement...' : 'Ajoutez des amis en tapant sur le bouton à droite !'}</Text>}
                renderItem={({ item }) => <UserBar {...item} />}
                keyExtractor={item => item.id}
            />
        );
    }
}

class UserBar extends React.PureComponent {
    profil = _ => Actions.push('profile', this.props);

    call = _ => Actions.push('call', this.props);

    goToFriend = id => {
        Actions.push('friend', {
            ...this.props,
            friendId: id,
            invitationId: this.props.invitation,
            isFriend: this.props.statut == 1,
            invitedMe: this.props.statut == 0 && this.props.asked == false,
        });
    };

    render() {
        return (
            <View style={styles.UserBar}>
                <TouchableNativeFeedback onPress={_ => this.goToFriend(this.props.id)} background={TouchableNativeFeedback.SelectableBackground()}>
                    <View style={styles.UserBarName}>
                        <View style={styles.UserBarBloc}>
                            <Avatar style={styles.UserBaravatar} iconSize={25} img={this.props.avatar && this.props.avatar['50']} />
                            <Text style={styles.UserBarTitle} numberOfLines={1}>
                                {this.props.prenom} {this.props.nom}
                            </Text>
                        </View>

                        {this.props.statut == 0 && this.props.asked == false ? (
                            <Button onPress={_ => this.goToFriend(this.props.id)} text='Invitation reçu' color='blue' style={styles.voirButton} />
                        ) : null}

                    </View>
                </TouchableNativeFeedback>
            </View>
        );
    }
}
