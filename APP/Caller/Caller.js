import React from 'react';
import { Alert, Animated, StatusBar, Easing, Text, TouchableWithoutFeedback, TouchableNativeFeedback, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { LinearGradient } from 'expo-linear-gradient';
import firebase from 'react-native-firebase';

import { MaterialIcons } from '@expo/vector-icons';

import Avatar from '../Basic/Avatar';

import { Immersive } from 'react-native-immersive';

import {
    RTCPeerConnection,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStream,
    MediaStreamTrack,
    mediaDevices,
    registerGlobals,
} from 'react-native-webrtc';

import USER_API from '../../UTILS/User';
import styles from './Style';

/**
 * ? ----------------------- ?
 * ? ETAT POSSIBLE DU CALLER ?
 * ? ----------------------- ?
 */
const STATE_WAITING = 1;
const STATE_RECEIVING = 2;
const STATE_CALLING = 3;
const STATE_ENDING = 4;

const ICE_SERVER = [
    { url: 'stun:stun3.l.google.com:19302' },
    { url: 'stun:stun.l.google.com:19302' },
    { url: 'stun:stun1.l.google.com:19302' },
    { url: 'stun:stun2.l.google.com:19302' },
    { url: 'stun:stun4.l.google.com:19302' },
    { url: 'stun:stun01.sipphone.com' },
    { url: 'stun:stun.ekiga.net' },
    { url: 'stun:stun.fwdnet.net' },
    { url: 'stun:stun.ideasip.com' },
    { url: 'stun:stun.iptel.org' },
    { url: 'stun:stun.rixtelecom.se' },
    { url: 'stun:stun.schlund.de' },
    { url: 'stun:stunserver.org' },
    { url: 'stun:stun.softjoys.com' },
    { url: 'stun:stun.voiparound.com' },
    { url: 'stun:stun.voipbuster.com' },
    { url: 'stun:stun.voipstunt.com' },
    { url: 'stun:stun.voxgratia.org' },
    { url: 'stun:stun.xten.com' }
]


export default class Caller extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            actualState: STATE_WAITING,
            micEnable: true,
            cameraEnable: true,
            faceCamera: true,
            overlayOpacity: new Animated.Value(1),
            overlayShow: true,
            localStream: null,
            remoteStream: null,
            waitingSubText: 'Connexion aux serveurs...',
            pendingCandidates: [],
            time: 1,
        };

        this.firstBubble = new Animated.Value(0);
        this.secondBubble = new Animated.Value(0);
    }

    PEER = null;

    lastCandidate = null;

    startLocalStream = _ => {
        return new Promise((resolve, reject) => {
            mediaDevices
                .enumerateDevices()
                .then(devices => {
                    const videoSourceId = devices.find(
                        sourceInfo => sourceInfo.kind == 'videoinput' && sourceInfo.facing == (this.state.faceCamera ? 'front' : 'environment'),
                    );
                    const facingMode = this.state.faceCamera ? 'user' : 'environment';

                    const constraints = {
                        audio: true,
                        video: {
                            mandatory: {
                                minWidth: 500,
                                minHeight: 300,
                                minFrameRate: 15,
                            },
                            facingMode,
                            optional: videoSourceId ? [{ sourceId: videoSourceId }] : [],
                        },
                    };

                    mediaDevices
                        .getUserMedia(constraints)
                        .then(newStream => {
                            this.setState({ localStream: newStream }, resolve);
                        })
                        .catch(err => {
                            console.error(err);
                            reject("Erreur lors de l'accès a la caméra (mediaDevices.getUserMedia)");
                        });
                })
                .catch(err => {
                    console.error(err);
                    reject("Erreur lors de l'accès aux Devices (mediaDevices.enumerateDevices)");
                });
        });
    };

    createPeerConnection = _ => {
        const PEER = new RTCPeerConnection({ iceServers: ICE_SERVER });

        PEER.onicecandidate = e => {
            const { candidate } = e;
            if (candidate) {
                console.info("[PEER onicecandidate] Candidate added")
                this.setState({
                    pendingCandidates: [...this.state.pendingCandidates, candidate]
                });
            }
        };

        // On a ajouté un stream
        // Donc on créer l'offre et on l'envoit
        PEER.onnegotiationneeded = async e => {

            if(this.props.type != "receiving"){

                // On créer une offre
                const offer = await this.PEER.createOffer();
                await this.PEER.setLocalDescription(offer);

                // Qu'on envoit le plus tot
                USER_API.callFriend(this.props.id, this.props.type, offer)
                    .then(_ => {
                        this.setState({ waitingSubText: 'Sonnerie en cours...' });
                    })
                    .catch(err => {
                        Alert.alert('CAller', 'ERREUR AVEC L\'ENVOIT DU PAYLOAD', err);
                    });
            }

        };

        PEER.oniceconnectionstatechange = e => {

            const signalingState =  e.target.signalingState
            const iceGatheringState =  e.target.iceGatheringState
            const iceConnectionState =  e.target.iceConnectionState

            console.info("[PEER oniceconnectionstatechange]",
                {
                    signalingState: signalingState,
                    iceGatheringState: iceGatheringState,
                    iceConnectionState: iceConnectionState
                }
            )

            // Si la liaison attent les pacquets du correspondant
            // On lui indique par quel serveur passer
            if(iceGatheringState == "gathering" && iceConnectionState == "checking") {
                this.sendCandidate()
            }

            switch (e.target.iceConnectionState) {
                case 'closed':
                case 'disconnected':
                case 'failed':
                    if (this.PEER) {
                        this.PEER.close();
                        this.setState({ remoteStream: null, localStream: null });
                    }
                    break;
            }
        };

        PEER.onaddstream = e => {

            // TODO : La bonne gestion d'un nouveau flux

            this.setState({
                remoteStream: e.stream,
                // actualState: STATE_CALLING,
            });
        };

        this.PEER = PEER;
    };

    sendCandidate = _ => {
        USER_API.answerFriend(this.props.id, 'candidate', this.state.pendingCandidates)
            .then(_ => {
            })
            .catch(err => {
                Alert.alert('CAller', 'ERREUR AVEC L\'ENVOIT DU PAYLOAD', err);
            });
    }

    async componentDidMount() {

        Immersive.on();
        Immersive.addImmersiveListener(Immersive.on);

        /**
         * ? ------------------- ?
         * ? Emission d'un appel ?
         * ? ------------------- ?
         */
        if (['video', 'audio'].includes(this.props.type)) {

            this.setState({ actualState: STATE_WAITING });

            this.bubleZoom();

            // Création de la liaison
            this.createPeerConnection()

            // Ajout caméra à la liaison
            // TODO : Audio si uniquement audio
            this.startLocalStream().then(async _ => {

                // En ajoutant le stream
                // On finitialise la liaison
                this.PEER.addStream(this.state.localStream);

            })


            /**
             * ? -------------------- ?
             * ? Reception d'un appel ?
             * ? -------------------- ?
             */
        } else if (this.props.type == 'receiving') {

            this.setState({ actualState: STATE_RECEIVING });

            this.createPeerConnection();

            const payload = JSON.parse(this.props.RTCdescription.data);
            await this.PEER.setRemoteDescription(new RTCSessionDescription(payload));

        }

        /**
         * ? ----------------- ?
         * ? Dans les deux cas ?
         * ? ----------------- ?
         */
        firebase.messaging().onMessage(message => {

            console.info(`[FCM] Type : ${message._data.type}`)

            const payload = JSON.parse(message._data.data);

            switch (message._data.type) {

                // Le correspondant accepte l'appel 
                case "takeCall":

                    this.setState({ actualState: STATE_CALLING });
                    this.PEER.setRemoteDescription(new RTCSessionDescription(payload))
                    
                    break;

                case "refuseCall":
                    Alert.alert("Caller", "Appel annulé par votre correspondant.", [{ text: 'Ok', onPress: this.endCall }])
                    break;

                case "candidate":
                    for (var cand of payload) {
                        this.PEER.addIceCandidate(new RTCIceCandidate(cand))
                    }
                    break;
            }

        })

    }

    componentWillUnmount() {
        if (this.PEER) {
            this.PEER.close();
        }
    }

    onTurnCamera = () => {
        this.setState({ faceCamera: !this.state.faceCamera });

        this.state.localStream.getVideoTracks().forEach(track => {
            track._switchCamera();
        });
    };

    RECEIVING_takeCall = () => {

        this.startLocalStream().then(async _ => {

            this.PEER.addStream(this.state.localStream);

            const answer = await this.PEER.createAnswer();
            await this.PEER.setLocalDescription(answer);

            this.setState({ actualState: STATE_CALLING });

            USER_API.answerFriend(this.props.id, 'takeCall', answer)
                .then(_ => {
                })
                .catch(err => {
                    Alert.alert('CAller', 'ERREUR AVEC L\'ENVOIT DU PAYLOAD', err);
                });

        })

        
    };

    RECEIVING_refuseCall = _ => {
        USER_API.answerFriend(this.props.id, 'refuseCall', { refuse: true }).then(_ => { })
        this.endCall()
    };

    endCall = () => {
        Immersive.off();
        Actions.pop();
    };

    onToggleCamera = () => {
        this.setState({ cameraEnable: !this.state.cameraEnable });
    };

    onToggleVoice = () => {
        this.setState({ micEnable: !this.state.micEnable });
    };

    overlayChange = () => {
        if (this.state.overlayShow) {
            Animated.timing(this.state.overlayOpacity, {
                toValue: 0,
                duration: 300,
            }).start(_ => {
                this.setState({ overlayShow: false });
            });
        } else {
            this.setState({ overlayShow: true }, _ => {
                Animated.timing(this.state.overlayOpacity, {
                    toValue: 1,
                    duration: 300,
                }).start();
            });
        }
    };

    bubleZoom = async _ => {
        Animated.timing(this.firstBubble, {
            toValue: 1,
            duration: 1000,
            easing: Easing.inOut(Easing.quad),
            useNativeDriver: true,
        }).start(_ => {
            Animated.timing(this.firstBubble, {
                toValue: 0.4,
                duration: 1000,
                easing: Easing.inOut(Easing.quad),
                useNativeDriver: true,
            }).start();
        });

        Animated.timing(this.secondBubble, {
            toValue: 1,
            duration: 1000,
            delay: 500,
            easing: Easing.inOut(Easing.quad),
            useNativeDriver: true,
        }).start(_ => {
            Animated.timing(this.secondBubble, {
                toValue: 0.3,
                duration: 1000,
                easing: Easing.inOut(Easing.quad),
                useNativeDriver: true,
            }).start(_ => {
                this.bubleZoom();
            });
        });
    };

    renderOverlay() {
        return (
            <Animated.View style={{ ...styles.Overlay, opacity: this.state.overlayOpacity }} zIndex={1}>
                <LinearGradient style={styles.Overlay} colors={['rgba(0,0,0,0.5)', 'transparent', 'transparent', 'rgba(0,0,0,0.5)']}>
                    <View style={styles.Header}>
                        <View style={styles.LeftHeader}>
                            <Avatar style={styles.HeaderUserAvatar} fast iconSize={25} img={this.props.avatar['50']} />
                            <View style={styles.Column}>
                                <Text style={styles.HeaderUserName}>
                                    {this.props.prenom} {this.props.nom}
                                </Text>
                                <Text style={styles.HeaderTiming}>{this.state.time} sec</Text>
                            </View>
                        </View>

                        <View style={styles.RightHeader}>
                            {this.state.cameraEnable ? (
                                <CallerButton
                                    style={{ marginTop: 0 }}
                                    onPress={this.onTurnCamera}
                                    icon={this.state.faceCamera ? 'camera-rear' : 'camera-front'}
                                    backgroundColor='transparent'
                                    iconColor='white'
                                />
                            ) : null}
                        </View>
                    </View>

                    <View style={styles.Footer}>
                        <CallerButton onPress={this.onToggleVoice} icon={this.state.micEnable ? 'mic' : 'mic-off'} iconColor='black' />
                        <CallerButton onPress={this.onToggleCamera} icon={this.state.cameraEnable ? 'videocam' : 'videocam-off'} iconColor='black' />
                        <CallerButton onPress={this.endCall} icon='call-end' backgroundColor='red' iconColor='white' />
                    </View>
                </LinearGradient>
            </Animated.View>
        );
    }

    renderCalling() {
        return (
            <>
                <Text> </Text>
                {this.state.overlayShow ? this.renderOverlay() : null}
                {this.state.localStream ? <RTCView
                    mirror={this.state.faceCamera}
                    style={{ flex: 1 }}
                    objectFit={'cover'}
                    streamURL={this.state.localStream.toURL()}
                /> : null}
                {this.state.remoteStream ? <RTCView
                    style={{ flex: 1 }}
                    objectFit={'cover'}
                    streamURL={this.state.remoteStream.toURL()}
                /> : null}
            </> /* */
        );
    }

    renderWaiting() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={styles.waitingAvatarBox}>
                        <Animated.View
                            style={{
                                ...styles.bubbleAvatar,
                                transform: [
                                    {
                                        scaleX: this.firstBubble,
                                        scaleY: this.firstBubble,
                                        perspective: 1000,
                                    },
                                ],
                            }}></Animated.View>
                        <Animated.View
                            style={{
                                ...styles.bubbleAvatar,
                                transform: [
                                    {
                                        scaleX: this.secondBubble,
                                        scaleY: this.secondBubble,
                                        perspective: 1000,
                                    },
                                ],
                            }}></Animated.View>

                        <Avatar style={styles.waitingAvatar} fast iconSize={75} img={this.props.avatar['300']} />
                    </View>
                    <Text style={styles.waitingText}>
                        {this.props.prenom} {this.props.nom}
                    </Text>
                    <Text style={styles.waitingSubText}>{this.state.waitingSubText}</Text>
                </View>

                <CallerButton onPress={this.endCall} icon='call-end' style={{ marginBottom: 15 }} backgroundColor='red' iconColor='white' />
            </View>
        );
    }

    renderReceiving() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={styles.waitingAvatarBox}>
                        <Avatar style={styles.waitingAvatar} fast iconSize={75} img={this.props.avatar['300']} />
                    </View>
                    <Text style={styles.waitingSubText}>Vous recevez un appel {"this.props._data.type"} de</Text>
                    <Text style={styles.waitingText}>
                        {this.props.prenom} {this.props.nom}
                    </Text>
                </View>

                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: 15 }}>
                    <CallerButton onPress={this.RECEIVING_takeCall} style={{ marginRight: '25%' }} icon='call' backgroundColor='green' iconColor='white' />
                    <CallerButton onPress={this.RECEIVING_refuseCall} icon='call-end' backgroundColor='red' iconColor='white' />
                </View>
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar translucent backgroundColor='rgba(255, 255, 255, 0) ' />
                <TouchableWithoutFeedback onPress={this.overlayChange}>
                    <View style={styles.Backdrop}>
                        {this.state.actualState == STATE_WAITING ? this.renderWaiting() : null}
                        {this.state.actualState == STATE_CALLING ? this.renderCalling() : null}
                        {this.state.actualState == STATE_ENDING ? this.renderEnding() : null}
                        {this.state.actualState == STATE_RECEIVING ? this.renderReceiving() : null}
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

/**
 * ?
 * ? CLASSE POUR FAIRE LES BOUTONS RONDS
 * ?
 */

class CallerButton extends React.PureComponent {
    render() {
        var style = { ...styles.OutsideBoxIcon, backgroundColor: this.props.backgroundColor || 'white' };

        style = this.props.backgroundColor == 'transparent' ? { ...style, elevation: 0 } : style;

        style = { ...style, ...this.props.style };

        return (
            <View style={style}>
                <TouchableNativeFeedback
                    style={styles.InsideBoxIcon}
                    onPress={this.props.onPress}
                    background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                    <View style={styles.InsideBoxIcon}>
                        <MaterialIcons style={styles.Icon} name={this.props.icon} size={25} color={this.props.iconColor} />
                    </View>
                </TouchableNativeFeedback>
            </View>
        );
    }
}
