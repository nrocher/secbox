import React from 'react';
import { Alert, Text, TouchableNativeFeedback, ScrollView, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { MaterialIcons } from '@expo/vector-icons';

import styles from './Style';
import Input from '../Basic/Input';
import Button from '../Basic/Button';

import USER_API from '../../UTILS/User';

export default class ChangePassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    goBack = _ => Actions.pop();

    save = _ => {
        if (
            this.password.value() != '' &&
            this.newpassword.value() != '' &&
            this.password.value() != this.newpassword.value() &&
            this.newpassword.value().length > 5
        ) {
            USER_API.changePassword(this.password.value(), this.newpassword.value())
                .then(() => {
                    Alert.alert('Changer de mot de passe', 'Le mot de passe a bien été modifié.');
                })
                .catch(err => {
                    Alert.alert('Changer de mot de passe', err);
                });
        }
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.Header}>
                    <TouchableNativeFeedback onPress={this.goBack} background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                        <View style={styles.HeaderIcon}>
                            <MaterialIcons name='chevron-left' size={25} />
                        </View>
                    </TouchableNativeFeedback>
                    <Text style={styles.HeaderText}>Informations générales</Text>

                    <View style={styles.HeaderIcon}></View>
                </View>

                <Input
                    name='Mot de passe actuel'
                    value={this.props.password}
                    placeholder='************'
                    ref={input => (this.password = input)}
                    returnKeyType={'next'}
                    onSubmitEditing={_ => this.newpassword.focus()}
                    secureTextEntry={true}
                />

                <Input
                    name='Nouveau mot de passe'
                    value={this.props.newpassword}
                    placeholder='************'
                    ref={input => (this.newpassword = input)}
                    secureTextEntry={true}
                    returnKeyType={'done'}
                />

                <Button onPress={this.save} text='Enregister' />
            </ScrollView>
        );
    }
}
