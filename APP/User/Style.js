import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        width: '100%',
        minHeight: '100%',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    Header: {
        flexDirection: 'row',
        width: '100%',
        padding: 15,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    HeaderText: {
        marginLeft: 15,
        fontFamily: 'open-sans-bold',
        fontSize: 17,
        color: '#000',
    },
    HeaderIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 35,
        height: 35,
    },
    LeftItem: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    /** CHANGE AVATAR ICON */
    Camera: {
        height: 300,
        width: 300,
        borderRadius: 300,
        overflow: 'hidden',
        marginBottom: 25,
    },
    CenterContent: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    infoText: {
        fontSize: 15,
        fontFamily: 'open-sans-regular',
        marginBottom: 5,
        textAlign:"center",
        maxWidth: 350,
    },
    blue: {
        color: '#2487E9',
    },
    captureButton: {
        marginTop: 20,
    },


    SettingsLink: {
        width: "90%",
        marginVertical:7.5
    },
    
    DeleteLinkText: {
        color:"red",
    }

});
