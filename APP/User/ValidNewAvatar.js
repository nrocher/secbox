import React from 'react';
import { Alert, Text, Image, BackHandler, TouchableNativeFeedback, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { MaterialIcons } from '@expo/vector-icons';
import USER_API from '../../UTILS/User';

import styles from './Style';

export default class ValidNewAvatar extends React.Component {

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.cancel);
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    cancel = _ => {
        Alert.alert('Modifier son avatar', "Êtes-vous sur d'annuler la modification de votre avatar ?", [
            { text: 'Annuler' },
            { text: 'Oui', onPress: () => Actions.pop() },
        ]);
        return true;
    };


    // TODO : Une progress bar pour l'upload de la photo ou au moins une information
    uploadProgress = p => {
        console.log('PROGRESS', p);
    };

    valid = _ => {
        USER_API.uploadAvatar(this.props.image, this.uploadProgress)
            .then(_ => {
                Actions.pop();
                Actions.refs.home.freshAvatar()
            })
            .catch(err => {
                Alert.alert('Modifier son avatar', "Erreur lors de l'envoit.\nRéessayer.\n" + (err.message || err));
            });
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header}>
                    <TouchableNativeFeedback onPress={this.cancel} background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                        <View style={styles.HeaderIcon}>
                            <MaterialIcons name='close' size={25} />
                        </View>
                    </TouchableNativeFeedback>
                    <Text style={styles.HeaderText}>Modifier son avatar</Text>
                    <TouchableNativeFeedback onPress={this.valid} background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                        <View style={styles.HeaderIcon}>
                            <MaterialIcons style={styles.blue} name='done' size={25} />
                        </View>
                    </TouchableNativeFeedback>
                </View>

                <View style={styles.CenterContent}>
                    <Image source={{ uri: this.props.uri }} style={styles.Camera} />
                    <Text style={styles.infoText}>
                        Pour choisir cette image comme avatar, veuillez cliquer sur le bouton valider en haut à droite.
                    </Text>
                    <Text style={styles.infoText}>
                        [TODO] Vous pouvez zoomer et recadrer la photo en pincant puis en déplacant l'image.
                        https://github.com/justhive/react-native-view-editor
                    </Text>
                </View>
            </View>
        );
    }
}
