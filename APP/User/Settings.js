import React from 'react';
import { Alert, Text, TouchableNativeFeedback, ScrollView, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Dialog from 'react-native-dialog';

import { MaterialIcons } from '@expo/vector-icons';

import styles from './Style';
import Button from '../Basic/Button';
import Link from '../Basic/Link';

import USER_API from '../../UTILS/User';

export default class Settings extends React.PureComponent {
    state = {
        deletionFinalStep: false,
        deletionPass: '',
    };

    goBack = _ => {
        Actions.pop();
        Actions.refs.home.loadUserData();
    };

    goToInfo = _ => Actions.push('changeGeneralInformation', this.props);

    goToAvatar = _ => Actions.push('changeAvatar');

    goToPassword = _ => Actions.push('changePassword');

    // TODO : Faire la page de confidentialités
    goToPrivacy = _ => Alert.alert('Paramètres', 'TODO : Faire la page de confidentialités');

    logout = _ => USER_API.logout();

    deleteAccount = (etape = 0) => {
        if (etape == 0) {
            Alert.alert('Supprimer mon compte', 'Vous commencez le processus de suppression de compte.\n\nCliquez sur OUI pour continuer.', [
                { text: 'Annuler' },
                { text: 'Oui', onPress: () => this.deleteAccount(1) },
            ]);
        } else if (etape == 1) {
            Alert.alert('Supprimer mon compte', 'Toute les données relatives à ce compte seront supprimées.\n\nCliquez sur OUI pour continuer.', [
                { text: 'Annuler' },
                { text: 'Oui', onPress: () => this.deleteAccount(2) },
            ]);
        } else if (etape == 2) {
            Alert.alert('Supprimer mon compte', 'Vous ne pourrez plus récupérer une quelconque donnée.\n\nCliquez sur OUI pour continuer.', [
                { text: 'Annuler' },
                { text: 'Oui', onPress: () => this.deleteAccount(3) },
            ]);
        } else if (etape == 3) {
            Alert.alert('Supprimer mon compte', 'Cette dernière étape supprimera votre compte.\n\nCliquez sur OUI pour continuer.', [
                { text: 'Annuler' },
                { text: 'Oui', onPress: () => this.setState({ deletionFinalStep: true }) },
            ]);
        }
    };

    changePassword = val => this.setState({ deletionPass: val });

    finalDeleteAccount = _ => {
        USER_API.deleteAccount(this.state.deletionPass)
            .then(_ => {})
            .catch(err => {
                this.setState({ deletionFinalStep: false, deletionPass: '' });
                Alert.alert('Supprimer mon compte', err);
            });
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.Header}>
                    <TouchableNativeFeedback onPress={this.goBack} background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                        <View style={styles.HeaderIcon}>
                            <MaterialIcons name='chevron-left' size={25} />
                        </View>
                    </TouchableNativeFeedback>
                    <Text style={styles.HeaderText}>Paramètres</Text>
                    <View style={styles.HeaderIcon}></View>
                </View>

                <Dialog.Container visible={this.state.deletionFinalStep}>
                    <Dialog.Title style={{ color: 'red' }}>Supprimer mon compte</Dialog.Title>
                    <Dialog.Description>Veuillez entrer votre mot de passe :</Dialog.Description>
                    <Dialog.Input
                        value={this.state.deletionPass}
                        onChangeText={this.changePassword}
                        placeholder='**********'
                        secureTextEntry={true}
                    />
                    <Dialog.Button onPress={_ => this.setState({ deletionFinalStep: false })} label='Annuler' />
                    <Dialog.Button onPress={this.finalDeleteAccount} label='SUPPRIMER' />
                </Dialog.Container>

                <Button onPress={this.goToInfo} style={{ ...styles.SettingsLink, marginTop: 0 }} icon='person' text='Informations générales' />
                <Button onPress={this.goToAvatar} style={styles.SettingsLink} icon='camera-alt' text="Changer d'avatar" />
                <Button onPress={this.goToPassword} style={styles.SettingsLink} icon='lock' text='Changer de mot de passe' />
                <Button onPress={this.goToPrivacy} style={styles.SettingsLink} icon='security' text='Paramètres de confidentialités' />
                <Button onPress={this.logout} style={styles.SettingsLink} icon='exit-to-app' text='Déconnexion' />
                <Link
                    onPress={_ => this.deleteAccount(0)}
                    style={styles.DeleteLink}
                    icon='warning'
                    textStyle={styles.DeleteLinkText}
                    text='Supprimer mon compte'
                />
            </ScrollView>
        );
    }
}
