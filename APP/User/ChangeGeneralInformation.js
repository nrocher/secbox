import React from 'react';
import { Alert, Text, TouchableNativeFeedback, ScrollView, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { MaterialIcons } from '@expo/vector-icons';

import styles from './Style';
import Button from '../Basic/Button';
import Input from '../Basic/Input';

import USER_API from '../../UTILS/User';

export default class ChangeGeneralInformation extends React.PureComponent {
    goBack = _ => Actions.pop();

    save = _ => {
        if (this.nom.value() != '' && this.prenom.value() != '' && this.pseudo.value() != '') {
            USER_API.modifyUserInfo(this.prenom.value(), this.nom.value(), this.pseudo.value(), this.bio.value())
                .then(() => {
                    Alert.alert('Informations Générales', 'Profil mis à jour.');
                })
                .catch(err => {
                    Alert.alert('Informations Générales', err);
                });
        }
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.Header}>
                    <TouchableNativeFeedback onPress={this.goBack} background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                        <View style={styles.HeaderIcon}>
                            <MaterialIcons name='chevron-left' size={25} />
                        </View>
                    </TouchableNativeFeedback>
                    <Text style={styles.HeaderText}>Informations générales</Text>

                    <View style={styles.HeaderIcon}></View>
                </View>

                <Input name='Prenom' defaultValue={this.props.userInfo.prenom} ref={input => (this.prenom = input)} returnKeyType={'done'} />

                <Input
                    name='Nom'
                    defaultValue={this.props.userInfo.nom}
                    ref={input => (this.nom = input)}
                    onWriteEnd={this.showSaveButton}
                    returnKeyType={'done'}
                />

                <Input name='Pseudo' defaultValue={this.props.userInfo.pseudo} ref={input => (this.pseudo = input)} returnKeyType={'done'} />

                <Input
                    name='Bio'
                    defaultValue={this.props.userInfo.bio}
                    multiline={true}
                    maxLength={160}
                    ref={input => (this.bio = input)}
                    returnKeyType={'done'}
                />

                <Button onPress={this.save} text='Enregister' />
            </ScrollView>
        );
    }
}
