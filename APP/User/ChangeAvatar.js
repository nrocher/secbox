import React from 'react';
import { Text, TouchableNativeFeedback, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { RNCamera } from 'react-native-camera';
import ImagePicker from 'react-native-image-picker';

import { MaterialIcons } from '@expo/vector-icons';

import styles from './Style';
import Button from '../Basic/Button';

export default class ChangeAvatar extends React.Component {
    constructor(props) {
        super(props);
    }

    goBack = _ => Actions.pop();

    captureAvatar = _ => {
        this.camera
            .takePictureAsync({ mirrorImage: true })
            .then(newAvatar => {
                Actions.replace('validNewAvatar', {
                    image: newAvatar,
                    uri: newAvatar.uri,
                    height: newAvatar.height,
                    width: newAvatar.width,
                    pictureOrientation: newAvatar.pictureOrientation,
                });
            })
            .catch(err => {});
    };

    openFolder = _ => {
        const options = {
            title: 'Selectionner votre avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.launchImageLibrary(options, newAvatar => {
            Actions.replace('validNewAvatar', {
                image: newAvatar,
                uri: newAvatar.uri,
                height: newAvatar.height,
                width: newAvatar.width,
                pictureOrientation: newAvatar.originalRotation,
            });
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.Header}>
                    <TouchableNativeFeedback onPress={this.goBack} background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                        <View style={styles.HeaderIcon}>
                            <MaterialIcons name='chevron-left' size={25} />
                        </View>
                    </TouchableNativeFeedback>
                    <Text style={styles.HeaderText}>Modifier son avatar</Text>
                    <TouchableNativeFeedback onPress={this.openFolder} background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                        <View style={styles.HeaderIcon}>
                            <MaterialIcons name='folder-open' size={25} />
                        </View>
                    </TouchableNativeFeedback>
                </View>

                <View style={styles.CenterContent}>
                    <RNCamera
                        ref={ref => (this.camera = ref)}
                        type={RNCamera.Constants.Type.front}
                        style={styles.Camera}
                        onFacesDetected={e => {
                            // console.log(e);
                        }}></RNCamera>
                    <Text style={styles.infoText}>Placez votre tête au milieu du cercle</Text>
                    <Text style={styles.infoText}>Faites en sorte d'être joli(e) !</Text>
                    <Button style={styles.captureButton} onPress={this.captureAvatar} text='Capturer' />
                </View>
            </View>
        );
    }
}
