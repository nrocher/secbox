import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

import styles from './Style';

export default class Button extends React.PureComponent {
    render() {
        var style = this.props.pressed ? { ...styles.button, ...styles.buttonClicked } : styles.button;
        style = { ...style, ...this.props.style };
        var textStyle = this.props.pressed ? { ...styles.buttonText, ...styles.buttonTextClicked } : styles.buttonText;

        let color;

        switch (this.props.color) {
            case 'red':
                color = '#D22A2A';
                break;
            case 'blue':
                color = '#2487E9';
                break;
            case 'green':
                color = '#2e8b57';
                break;
            default:
                color = '#2B2B2B';
                break;
        }

        textStyle = { ...textStyle, color: color };
       
        if (this.props.disabled) {
            style = { ...styles.button, ...styles.disabledButton, ...this.props.style };
            textStyle = { ...textStyle, ...styles.disabledbuttonText };
        }

        if (this.props.icon) {
            style = { ...style, ...styles.buttonWithIcon };
        }


        return (
            <TouchableOpacity style={style} disabled={this.props.pressed || this.props.disabled} activeOpacity={0.8} onPress={this.props.onPress}>
                <>
                    {this.props.icon ? <MaterialIcons name={this.props.icon} size={25} color={color} style={styles.buttonIconStyle} /> : null}

                    <Text style={textStyle}>{this.props.pressed ? this.props.pressedText : this.props.text}</Text>
                </>
            </TouchableOpacity>
        );
    }
}
