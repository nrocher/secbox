import React from 'react';

import FastImage from 'react-native-fast-image';
import { MaterialIcons } from '@expo/vector-icons';

export default class Avatar extends React.PureComponent {
    render() {
        if (this.props.img) {
            return (
                <FastImage
                    style={this.props.style}
                    source={{
                        uri: this.props.img,
                        priority: this.props.fast ? FastImage.priority.high : FastImage.priority.normal,
                    }}
                />
            );
        } else {
            return <MaterialIcons name='person' size={this.props.iconSize} color='#424242' style={this.props.style} />;
        }
    }
}
