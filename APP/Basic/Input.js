import React from 'react';
import { Text, View, TextInput } from 'react-native';

import styles from './Style';

export default class Input extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.defaultValue || '',
            focus: false,
        };
    }

    timer;

    onChangeValue = val => {
        this.setState({ value: val });

        clearTimeout(this.timer);
        this.timer = setTimeout(_ => {
            if (this.props.onWriteEnd) this.props.onWriteEnd();
        }, 500);
    };

    focus = _ => this.input.focus();

    value = _ => this.state.value;

    reset = _ => this.setState({ value: '' });

    onFocus = _ => this.setState({ focus: true });

    onBlur = _ => this.setState({ focus: false });

    isValid = _ => {
        let v = this.state.value.replace(' ', '') != '';

        if (this.props.secureTextEntry == true) v = v && this.state.value.length > 5;

        if (this.props.keyboardType == 'email-address') v = v && validateEmail(this.state.value);

        return v;
    };

    render() {
        return (
            <View style={{ height: 'auto', width: '100%', alignItems: 'center' }}>
                <Text style={styles.label}>{this.props.name}</Text>
                <TextInput
                    onChangeText={this.onChangeValue}
                    value={this.state.value}
                    placeholder={this.props.placeholder}
                    style={this.state.focus ? { ...styles.input, ...styles.inputFocus } : styles.input}
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    ref={input => (this.input = input)}
                    {...this.props}
                />
            </View>
        );
    }
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
