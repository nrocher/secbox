import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    button: {   
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: '#fff',
        borderRadius: 5,
        elevation: 3,
    },
    disabledButton: {
        elevation: 0,
        borderColor: '#DCDCDC',
        borderWidth: 2,
    },
    disabledbuttonText: {
        color: '#DCDCDC',
    },
    buttonText: {
        fontFamily: 'open-sans-bold',
        color: '#2487E9',
        fontSize: 15,
    },
    buttonClicked: {
        elevation: 0,
        borderWidth: 1,
        borderColor: '#9E9E9E',
    },
    buttonTextClicked: {
        color: '#9E9E9E',
    },
    buttonWithIcon: {
        paddingLeft:15,
    },
    buttonIconStyle : {
        marginRight:15
    },




    input: {
        borderColor: '#DCDCDC',
        borderWidth: 2,
        borderRadius: 5,
        paddingVertical: 10,
        paddingHorizontal: 15,
        marginBottom: 15,
        width: '80%',
    },
    inputFocus: {
        borderColor: '#2487E9',
    },
    label: {
        fontFamily: 'open-sans-semibold',
        color: '#000',
        fontSize: 18,
        width: '80%',
        marginBottom: 10,
    },


    link: {
        paddingVertical: 10,
        borderRadius: 5,
        marginTop: 25,
        flexDirection: 'row'
    },
    widthLink: {
        width: '80%',
        marginTop: -15,
        marginBottom: 15,
    },
    linkText: {
        fontFamily: 'open-sans-regular',
        color: '#2487E9',
        fontSize: 13,
    },
    linkIcon: {
        marginRight: 10
    },
});
