import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

import styles from './Style';

export default class Link extends React.Component {
    render() {
        var style = this.props.inputWidth ? { ...styles.link, ...styles.widthLink } : styles.link;

        style = { ...style, ...this.props.style };

        var textStyle = { ...styles.linkText, ...this.props.textStyle };

        return (
            <TouchableOpacity style={style} activeOpacity={0.8} onPress={this.props.onPress}>
                {this.props.icon ? <MaterialIcons name={this.props.icon}  color={(this.props.textStyle && this.props.textStyle.color) || "grey"} size={18} style={{...styles.linkIcon, ...styles.textStyle}} /> : null}

                <Text style={textStyle}>{this.props.text}</Text>
            </TouchableOpacity>
        );
    }
}
