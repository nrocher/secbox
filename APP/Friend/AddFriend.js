import React from 'react';
import { Alert, Text, ScrollView, TouchableNativeFeedback, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Keyboard } from 'react-native';

import { MaterialIcons } from '@expo/vector-icons';

import USER_API from '../../UTILS/User';
import Avatar from '../Basic/Avatar';
import Button from '../Basic/Button';
import Input from '../Basic/Input';

import styles from './Style';

export default class AddFriend extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            avatar: {},
            nom: '',
            prenom: '',
            pseudo: '',
            id: '',
            button: false,
            fetching: false,
            adding: false,
        };
    }

    goBack = _ => Actions.pop();

    componentDidMount() {}

    // TODO : add nfc via react-native-nfc-manager

    verifierPseudo = _ => {
        if (!this.state.fetching) {
            this.setState({ fetching: true });
            USER_API.getUserViaPseudo(this.pseudo.value())
                .then(user => {
                    Keyboard.dismiss();
                    this.setState({ ...user, button: true, fetching: false });
                })
                .catch(err => {
                    if (err.err) {
                        Alert.alert('Ajouter un(e) ami(e)', err.err);
                    }
                    this.setState({ button: false, avatar: {}, fetching: false, id: 'int', nom: '', prenom: '' });
                });
        }
    };

    ajouter = _ => {
        if (!this.state.adding) {
            this.setState({ adding: true });
            USER_API.inviteFriend(this.state.id)
                .then(_ => {
                    Alert.alert('Ajouter un(e) ami(e)', 'Invitation envoyé à ' + this.state.prenom + ' !', [
                        {
                            text: 'Fermer',
                            onPress: () => {
                                Actions.pop();
                                Actions.refs.home.freshFriends();
                            },
                        },
                    ]);
                })
                .catch(err => {
                    Alert.alert('Ajouter un(e) ami(e)', err);
                    this.pseudo.reset();
                    this.setState({ button: false, avatar: {}, adding: false, id: '', nom: '', prenom: '' });
                });
        }
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.Header}>
                    <TouchableNativeFeedback onPress={this.goBack} background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                        <View style={styles.HeaderIcon}>
                            <MaterialIcons name='chevron-left' size={25} />
                        </View>
                    </TouchableNativeFeedback>
                    <Text style={styles.HeaderText}>Ajouter un(e) ami(e)</Text>
                    <View style={styles.HeaderIcon}></View>
                </View>

                <View style={styles.CenterContent}>
                    <Avatar style={styles.UserInfoAvatar} fast iconSize={75} img={this.state.avatar['300']} />
                    <Text style={this.state.prenom != '' ? styles.UserInfoTitle : {}}>
                        {this.state.prenom} {this.state.nom}
                    </Text>
                    <Input
                        name='Pseudo'
                        placeholder='Pseudo de votre ami(e)'
                        ref={i => (this.pseudo = i)}
                        onSubmitEditing={this.verifierPseudo}
                        onWriteEnd={this.verifierPseudo}
                        returnKeyType={'done'}
                    />
                    <View style={styles.infoBlock}>
                        <MaterialIcons name='nfc' size={25} />
                        <Text style={styles.infoText}>[TODO] react-native-nfc-manager</Text>
                    </View>
                    <View style={styles.infoBlock}>
                        <MaterialIcons name='person' size={25} />
                        <Text style={styles.infoText}>Votre pseudo est : {this.props.pseudo}</Text>
                    </View>

                    {this.state.id != '' ? (
                        <Button
                            style={styles.captureButton}
                            disabled={!this.state.button}
                            onPress={this.ajouter}
                            color='blue'
                            text={this.state.button ? 'Ajouter' : 'Utilisateur introuvable'}
                        />
                    ) : null}
                </View>
            </ScrollView>
        );
    }
}
