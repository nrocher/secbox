import React from 'react';
import { Alert, Text, FlatList, TouchableNativeFeedback, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { MaterialIcons } from '@expo/vector-icons';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';

import USER_API from '../../UTILS/User';

import Avatar from '../Basic/Avatar';
import Button from '../Basic/Button';

import styles from './Style';

export default class Friend extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nom: '',
            prenom: '',
            avatar: {},
            id: '',
            pseudo: '',
            bio: '',
            history: [],
            loading: true,
            isFriend: true,
            invitedMe: false,
        };
    }

    _menu = null;

    componentDidMount() {
        this.loadUserData();
        this.setState({
            id: this.props.id,
            nom: this.props.nom,
            prenom: this.props.prenom,
            avatar: this.props.avatar,
            isFriend: this.props.isFriend,
            invitedMe: this.props.invitedMe,
        });
    }

    loadUserData = _ => {
        USER_API.getUserInformation(this.props.friendId)
            .then(user => {
                this.setState({ ...user, loading: false });
            })
            .catch(err => {
                Alert.alert('SecBox', err);
            });
    };

    setMenuRef = ref => {
        this._menu = ref;
    };

    showMenu = () => this._menu.show();

    // TODO : Remove this
    fresh = _ => {
        this.loadUserData();
    };

    goBack = _ => {
        Actions.pop();
        Actions.refs.home.freshFriends();
    };

    remove = _ => {
        this._menu.hide();

        USER_API.removeInvitation(this.props.invitationId)
            .then(_ => {
                Actions.pop();
                Actions.refs.home.freshFriends();
            })
            .catch(err => {
                Alert.alert('Invitation', err);
            });
    };

    accept = _ => {
        USER_API.acceptInvitation(true, this.props.invitationId)
            .then(_ => {
                this.setState({ isFriend: true, invitedMe: false });
            })
            .catch(err => {
                Alert.alert('Invitation', err);
            });
    };

    deny = _ => {
        USER_API.acceptInvitation(false, this.props.invitationId)
            .then(_ => {
                Actions.pop();
                Actions.refs.home.freshFriends();
            })
            .catch(err => {
                Alert.alert('Invitation', err);
            });
    };

    makeAudioCall = _ => {
        this._menu.hide();
        Actions.push('caller', { ...this.state, type: 'audio' });
    };

    makeVideoCall = _ => {
        this._menu.hide();
        Actions.push('caller', { ...this.state, type: 'video' });
    };

    render() {
        return (
            <FlatList
                contentContainerStyle={styles.container}
                ListHeaderComponentStyle={styles.ContainerHeader}
                data={this.state.history}
                ListHeaderComponent={_ => (
                    <>
                        <View style={styles.Header}>
                            <TouchableNativeFeedback onPress={this.goBack} background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                                <View style={styles.HeaderIcon}>
                                    <MaterialIcons name='chevron-left' size={25} />
                                </View>
                            </TouchableNativeFeedback>

                            <Menu
                                ref={this.setMenuRef}
                                button={
                                    <TouchableNativeFeedback
                                        onPress={this.showMenu}
                                        background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                                        <View style={styles.HeaderIcon}>
                                            <MaterialIcons name='more-vert' size={25} />
                                        </View>
                                    </TouchableNativeFeedback>
                                }>
                                <MenuItem onPress={this.makeAudioCall}>Appel audio</MenuItem>
                                <MenuItem onPress={this.makeVideoCall}>Appel vidéo</MenuItem>
                                <MenuDivider />
                                <MenuItem onPress={this.remove}>Supprimer cet ami(e)</MenuItem>
                            </Menu>
                        </View>

                        <View style={styles.UserInfo}>
                            <Avatar style={styles.UserInfoavatar} fast iconSize={75} img={this.state.avatar['300']} />
                            <Text style={styles.UserInfoTitle}>
                                {this.state.prenom} {this.state.nom}
                            </Text>
                            <Text style={styles.UserInfoPseudo}>{this.state.pseudo}</Text>
                            {this.state.bio != '' ? <Text style={styles.UserInfoPseudo}>{this.state.bio}</Text> : null}
                        </View>

                        <View style={styles.FriendsButtons}>
                            {this.state.isFriend ? (
                                <>
                                    <Button style={styles.MarginRight} icon='local-phone' onPress={this.makeAudioCall} text={'Appel audio'} />
                                    <Button
                                        style={styles.MarginLeft}
                                        color='blue'
                                        icon='videocam'
                                        onPress={this.makeVideoCall}
                                        text={'Appel vidéo'}
                                    />
                                </> /* */
                            ) : null}

                            {this.state.invitedMe ? (
                                <>
                                    <Button style={styles.MarginRight} color='red' icon='close' onPress={this.deny} text={'Refuser'} />
                                    <Button style={styles.MarginLeft} color='green' icon='done' onPress={this.accept} text={'Accepter'} />
                                </> /* */
                            ) : null}
                        </View>

                        <View style={styles.ListHeader}>
                            <Text style={styles.ListHeaderText}>Historique</Text>
                        </View>
                    </> /* */
                )}
                ListEmptyComponent={<Text>{this.state.loading ? 'Chargement...' : "Vous n'avez passé d'appel pour le moment."}</Text>}
                renderItem={({ item }) => <HistoryBar {...item} />}
                keyExtractor={item => item.id}
            />
        );
    }
}

class HistoryBar extends React.PureComponent {
    // TODO les bares d'historiques

    profil = _ => {
        Actions.push('profile', this.props);
    };

    call = _ => {
        Actions.push('call', this.props);
    };

    render() {
        return (
            <View style={styles.UserBar}>
                <TouchableNativeFeedback onPress={this.profil} background={TouchableNativeFeedback.SelectableBackground()}>
                    <View style={styles.UserBarName}>
                        <View style={styles.UserBarBloc}>
                            <Avatar style={styles.UserBaravatar} iconSize={25} img={this.props.avatar && this.props.avatar['50']} />
                            <Text style={styles.UserBarTitle}>
                                {this.props.prenom} {this.props.nom}
                            </Text>
                        </View>

                        <TouchableNativeFeedback onPress={this.call} background={TouchableNativeFeedback.SelectableBackgroundBorderless()}>
                            <View style={styles.HeaderIcon}>
                                <MaterialIcons name='local-phone' size={25} color='#424242' style={this.props.style} />
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                </TouchableNativeFeedback>
            </View>
        );
    }
}
