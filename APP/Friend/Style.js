import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        width: '100%',
        minHeight: '100%',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    Header: {
        flexDirection: 'row',
        width: '100%',
        padding: 15,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    HeaderText: {
        marginLeft: 15,
        fontFamily: 'open-sans-bold',
        fontSize: 17,
        color: '#000',
    },
    HeaderIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 35,
        height: 35,
    },
    CenterContent: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        width: '100%',
    },
    UserInfoAvatar: {
        width: 200,
        height: 200,
        textAlign: 'center',
        lineHeight: 200,
        borderRadius: 500,
        backgroundColor: '#E0E0E0',
    },
    UserInfoTitle: {
        fontFamily: 'open-sans-bold',
        fontSize: 20,
        color: '#000',
        marginTop: 10,
        marginBottom: 25,
    },
    infoBlock: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        width: '80%',
    },
    infoText: {
        fontSize: 15,
        marginLeft:15,
        fontFamily: 'open-sans-regular',
    },
    blue: {
        color: '#2487E9',
    },
    captureButton: {
        marginTop: 25,
    },






    ContainerHeader: {
        width: '100%',
        alignItems: 'center',
        backgroundColor: '#fff',
    },

    ListHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        paddingHorizontal: 15,
        marginBottom: 15,
    },
    ListHeaderText: {
        fontFamily: 'open-sans-semibold',
        fontSize: 18,
        color: '#000',
    },
    HeaderIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 40,
        height: 40,
    },




    UserInfoavatar: {
        width: 200,
        height: 200,
        textAlign: 'center',
        lineHeight: 200,
        borderRadius: 500,
        backgroundColor: '#E0E0E0',
    },
    UserInfo: {
        alignItems: 'center',
        flexDirection: 'column',
    },
    UserInfoTitle: {
        fontFamily: 'open-sans-bold',
        fontSize: 20,
        color: '#000',
        marginTop: 15,
    },
    UserInfoPseudo: {
        fontFamily: 'open-sans-regular',
        fontSize: 15,
        color: '#000',
        marginTop: 5,
    },



    FriendsButtons:{
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 20,
        marginBottom: 25,
    },
    MarginLeft:{
        marginLeft: 12.5
    },
    MarginRight: {
        marginRight: 12.5
    },




    UserBar: {
        flexDirection: 'row',
    },
    UserBarName: {
        width: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    UserBarBloc: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    UserBarTitle: {
        fontFamily: 'open-sans-bold',
        fontSize: 18,
        color: '#000',
        marginLeft: 15,
    },
    UserBaravatar: {
        borderRadius: 100,
        backgroundColor: '#E0E0E0',
        textAlign: 'center',
        height: 50,
        lineHeight: 50,
        width: 50,
    },
});
