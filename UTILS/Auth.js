import axios from 'axios';

const USER_API = 'https://europe-west1-secbox-f2233.cloudfunctions.net/user';

const ERROR = {
    FORM: 'Le formulaire ne répond pas au norme imposé.',

    U1: 'Email ou mot de passe faux.',
    U2: 'Cet email est déjà utilisé.',
    U3: 'Ce pseudo est déjà utilisé.',
    U4: 'Mot de passe invalide.',

    F1: "Vous êtes déjà ami ou l'invitation est en attente.",
    F2: "Cet utilisateur n'existe pas.",
    F3: 'Vous ne pouvez pas vous ajouter en ami.',
    F4: "Cette invitation n'existe pas.",

    A1: "Format d'image non accepté.",
    A2: "Résolution de l'image trop petite.",
};

function error(err) {
    console.error(err);
}

export default {
    login: (email, password) => {
        return new Promise((resolve, reject) => {
            axios
                .post(USER_API + '/login', {
                    email: email,
                    password: password,
                })
                .then(res => {
                    if (res.data.err) {
                        reject(ERROR[res.data.err]);
                    } else {
                        resolve(res.data.token);
                    }
                })
                .catch(err => {
                    error(err);
                    if (err.status == 400) {
                        reject(ERROR['FORM']);
                    } else {
                        reject('Veuillez vérifier votre connexion internet.');
                    }
                });
        });
    },
    signup: (email, pseudo, password, nom, prenom) => {
        const DATA = {
            email: email,
            nom: nom,
            pseudo: pseudo,
            prenom: prenom,
            password: password,
        };

        return new Promise((resolve, reject) => {
            axios
                .post(USER_API + '/signup', DATA)
                .then(res => {
                    if (res.data.err == false) {
                        resolve(true);
                    } else {
                        reject(ERROR[res.data.err]);
                    }
                })
                .catch(err => {
                    error(err);
                    if (err.status == 400) {
                        reject(ERROR['FORM']);
                    } else {
                        reject('Veuillez vérifier votre connexion internet.');
                    }
                });
        });
    },
    lostpassword: email => {
        return new Promise((resolve, reject) => {
            axios
                .post(USER_API + '/lostpassword', {
                    email: email,
                })
                .then(data => {
                    if (data.data.err) {
                        reject(data.data.err);
                    } else {
                        resolve(true);
                    }
                })
                .catch(err => {
                    error(err);
                    reject(err);
                });
        });
    },
};
