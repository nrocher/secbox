import axios from 'axios';
import { Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import RNSecureStorage from 'rn-secure-storage';

const DEBUG = false;

const BASE = DEBUG ? 'http://10.0.2.2:5001/secbox-f2233/europe-west1' : 'https://europe-west1-secbox-f2233.cloudfunctions.net';

const USER_API = BASE + '/user';

const FRIEND_API = BASE + '/friend';

const ERROR = {
	"FORM": "Le formulaire ne répond pas au norme imposé.",
	
	"U1" : "Email ou mot de passe faux.",
	"U2" : "Cet email est déjà utilisé.",
	"U3" : "Ce pseudo est déjà utilisé.",
	"U4" : "Mot de passe invalide.",

	"F1" : "Vous êtes déjà ami ou l'invitation est en attente.",
	"F2" : "Cet utilisateur n'existe pas.",
	"F3" : "Vous ne pouvez pas vous ajouter en ami.",
	"F4" : "Cette invitation n'existe pas.",
	"F5" : "Cette invitation n'existe pas.",
	"F6" : "L'utilisateur ne peut recevoir d'appel.",

	"A1" : "Format d'image non accepté.",
	"A2" : "Résolution de l'image trop petite."
}
var TOKEN = '';

function GetUserToken() {
    return new Promise((resolve, reject) => {
        if (TOKEN == '') {
            RNSecureStorage.get('token')
                .then(da => {
                    TOKEN = da;
                    resolve(TOKEN);
                })
                .catch(err => {
                    console.error(err);
                    console.dir(err);
                    Alert.alert('SecBox', 'Erreur système.');
                });
        } else {
            resolve(TOKEN);
        }
    });
}

const error = err => {
    console.error(err);
    console.dir(err);

    if (err.status == 401) {
        RNSecureStorage.remove('token');
        AsyncStorage.clear();
        Actions.reset('auth');

        return true;
    }
};

export default {
    getUserInformation: (userId = undefined) => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = USER_API + '/' + (userId != undefined ? userId : '');
                axios
                    .post(URL, {
                        token: token,
                    })
                    .then(res => {
                        AsyncStorage.setItem(URL, JSON.stringify(res.data));
                        resolve(res.data);
                    })
                    .catch(err => {
                        error(err);

                        if (err.status == 404) {
                            resolve({});
                        } else {
                            AsyncStorage.getItem(URL)
                                .then(data => {
                                    resolve(JSON.parse(data));
                                })
                                .catch(err => {
                                    reject('Veuillez vérifier votre connexion internet.');
                                });
                        }
                    });
            });
        });
    },

    deleteAccount: password => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = USER_API + '/delete';
                axios
                    .post(URL, {
                        token: token,
                        password: password,
                    })
                    .then(res => {
                        if (res.data.err) {
                            reject(ERROR[res.data.err]);
                        } else {
                            var err = { response: { status: 401 } };
                            error(err);
                        }
                    })
                    .catch(err => {
                        error(err);
                        reject({ err: 'Veuillez vérifier votre connexion internet.' });
                    });
            });
        });
    },

    changePassword: (actualPassword, newPassword) => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = USER_API + '/changepassword';
                axios
                    .post(URL, {
                        token: token,
                        password: actualPassword,
                        newpassword: newPassword,
                    })
                    .then(res => {
                        if (res.data.err) {
                            reject(ERROR[res.data.err]);
                        } else {
                            resolve();
                        }
                    })
                    .catch(err => {
                        error(err);
                        reject({ err: 'Veuillez vérifier votre connexion internet.' });
                    });
            });
        });
    },

    modifyUserInfo: (prenom, nom, pseudo, bio) => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = USER_API + '/edit';
                axios
                    .post(URL, {
                        token: token,
                        prenom: prenom,
                        nom: nom,
                        pseudo: pseudo,
                        bio: bio,
                    })
                    .then(res => {
                        if (res.data.err) {
                            reject(ERROR[res.data.err]);
                        } else {
                            resolve();
                        }
                    })
                    .catch(err => {
                        error(err);
                        reject({ err: 'Veuillez vérifier votre connexion internet.' });
                    });
            });
        });
    },

    updatePhoneId: phoneId => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = USER_API + '/updatePhoneId/';
                axios
                    .post(URL, {
                        token: token,
                        phone_id: phoneId
                    })
                    .then(res => {
                        resolve(res.data);
                    })
                    .catch(err => {
                        error(err);
                        reject({ err: 'Veuillez vérifier votre connexion internet.' });
                    });
            });
        });
    },

    getUserViaPseudo: pseudo => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = USER_API + '/pseudo/' + pseudo;
                axios
                    .post(URL, {
                        token: token,
                    })
                    .then(res => {
                        resolve(res.data);
                    })
                    .catch(err => {
                        error(err);

                        if (err.status == 404) {
                            reject({ err: false, find: false });
                        } else {
                            reject({ err: 'Veuillez vérifier votre connexion internet.' });
                        }
                    });
            });
        });
    },

    logout: () => {
        GetUserToken().then(token => {
            const URL = USER_API + '/logout/';
            axios
                .post(URL, {
                    token: token,
                })
                .then(res => {})
                .catch(err => {});
            var err = { response: { status: 401 } };
            error(err);
        });
    },

    callFriend: (friend, type, data) => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = FRIEND_API + '/call';
                axios
                    .post(URL, {
                        token: token,
                        user_asked: friend,
                        type: type,
                        data: JSON.stringify(data)
                    })
                    .then(res => {
                        if(res.data.err){
                            reject(ERROR[res.data.err]);
                        } else {
                            resolve();
                        }
                    })
                    .catch(err => {
                        error(err);
                        reject({ err: 'Veuillez vérifier votre connexion internet.' });
                    });
            });
        });
    },

    answerFriend: (friend, type, data) => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = FRIEND_API + '/answer';
                axios
                    .post(URL, {
                        token: token,
                        user_asking: friend,
                        type: type,
                        data: JSON.stringify(data)
                    })
                    .then(res => {
                        if(res.data.err){
                            reject(ERROR[res.data.err]);
                        } else {
                            resolve();
                        }
                    })
                    .catch(err => {
                        error(err);
                        reject({ err: 'Veuillez vérifier votre connexion internet.' });
                    });
            });
        });
    },


    getFriends: () => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = FRIEND_API + '/';
                axios
                    .post(URL, {
                        token: token,
                    })
                    .then(res => {
                        AsyncStorage.setItem(URL, JSON.stringify(res.data));
                        resolve(res.data);
                    })
                    .catch(err => {
                        error(err);

                        AsyncStorage.getItem(URL)
                            .then(data => {
                                resolve(JSON.parse(data));
                            })
                            .catch(err => {
                                reject('Veuillez vérifier votre connexion internet.');
                            });
                    });
            });
        });
    },

    inviteFriend: friendId => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = FRIEND_API + '/invite';
                axios
                    .post(URL, {
                        token: token,
                        user_asked: friendId,
                    })
                    .then(res => {
                        if (res.data.err == false) {
                            resolve();
                        } else {
                            reject(ERROR[res.data.err]);
                        }
                    })
                    .catch(err => {
                        error(err);

                        reject('Veuillez vérifier votre connexion internet.');
                    });
            });
        });
    },

    acceptInvitation: (accept, invitationId) => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = FRIEND_API + (accept ? '/accept' : '/deny');
                axios
                    .post(URL, {
                        token: token,
                        invitation: invitationId,
                    })
                    .then(res => {
                        if (res.data.err == false) {
                            resolve();
                        } else {
                            reject(ERROR[res.data.err]);
                        }
                    })
                    .catch(err => {
                        error(err);
                        reject('Veuillez vérifier votre connexion internet.');
                    });
            });
        });
    },

    removeInvitation: invitationId => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = FRIEND_API + '/remove';
                axios
                    .post(URL, {
                        token: token,
                        invitation: invitationId,
                    })
                    .then(res => {
                        if (res.data.err == false) {
                            resolve();
                        } else {
                            reject(ERROR[res.data.err]);
                        }
                    })
                    .catch(err => {
                        error(err);
                        reject('Veuillez vérifier votre connexion internet.');
                    });
            });
        });
    },

    uploadAvatar: (image, progress) => {
        return new Promise((resolve, reject) => {
            GetUserToken().then(token => {
                const URL = USER_API + '/avatar';
                let form = new FormData();
                form.append('token', token);
                form.append('avatar', { uri: image.uri, type: 'image/jpeg', name: token });

                axios
                    .post(URL, form, {
                        headers: {
                            'Content-Type': 'multipart/form-data',
                        },
                        onUploadProgress: progressEvent => {
                            let p = parseInt(Math.round((progressEvent.loaded / progressEvent.total) * 100));
                            progress(p);
                        },
                    })
                    .then(res => {
                        if (!res.data.err) {
                            resolve();
                        } else {
                            reject(ERROR[res.data.err]);
                        }
                    })
                    .catch(err => {
                        error(err);
                        reject('Veuillez vérifier votre connexion internet.');
                    });
            });
        });
    },
};
