import wakeUpApp from 'react-native-wakeup-screen';

import USER_API from './User';

export default async (message) => {
	// handle your message


	console.warn("[BACKGROUND] Reception d'un message FCM ")

	if(message.data.messageType == "call") {
		if(message.data.asking) {
		console.warn("[BACKGROUND] Reception d'un appel")
	
		USER_API.getUserInformation(message.data.asking)
			.then(user => {
				const yourObject = { action: 'caller', props: { RTCdescription: message.data, ...user, type: 'receiving' } };
	
				wakeUpApp({
					data: yourObject,
				});
			})
			.catch(err => {
			});
		}
	}

    return Promise.resolve();
}