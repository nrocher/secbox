import {AppRegistry} from 'react-native';

/**
 * ? Methode pour l'application
 */
import App from './App';
AppRegistry.registerComponent("secbox", () => App);

/**
 * ? Methode pour la gestion des messages en arrière plan
 */
import BackgroundMessage from './UTILS/BackgroundMessage';

AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => BackgroundMessage);